import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Header extends Component {

render(){
    return(
        <div className='header'>
          <Link className='link-style' to='/'> link Home </Link>
          <Link className='link-style' to='/contacts'> link Contacts </Link>
        </div>
    )
  }
}

export default Header;