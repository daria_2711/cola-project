import React, {Component} from 'react';
import Header from "./Header";
import Footer from "./Footer";
import Contacts from "./Contacts";
import {Route, Switch} from "react-router-dom";

class Home extends Component {

render(){
    return(
        <div>
          <Header />
            <Switch>
                <Route path='/contacts' component={Contacts} />
            </Switch>
          <Footer/>
        </div>
    )
  }
}

export default Home;